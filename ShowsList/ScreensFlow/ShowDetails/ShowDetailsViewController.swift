//
//  ShowDetailsViewController.swift
//
//  Created by Eslam Salem on 12/25/21.
//

import UIKit

class ShowDetailsViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    var viewModel: ShowDetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension ShowDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView()
    }
}

extension ShowDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        ShowDetailsCells.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCell = ShowDetailsCells.allCases[indexPath.row]
        switch currentCell {
        case .headerInfo:
            return configureHeaderInfoCell()
        default:
            return configureInfoCell(currentCell: currentCell)
        }
    }

    private func configureHeaderInfoCell() -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeCell() as ShowHeaderDataTableViewCell
        cell.setDataToCell(with: viewModel.showData)
        return cell
    }

    private func configureInfoCell(currentCell: ShowDetailsCells) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeCell() as ShowInfoTableViewCell
        cell.setDataToCell(
            title: currentCell.title,
            description: viewModel.getInfoDescription(currentCell: currentCell)
        )
        return cell
    }
}
