//
//  ShowDetailsViewModel.swift
//
//  Created by Eslam Salem on 12/25/21.
//

import Foundation

enum ShowDetailsCells: CaseIterable {
    case headerInfo
    case geners
    case language
    case type
    case status
    
    var title: String {
        switch self {
        case .headerInfo:
            return ""
        case .geners:
            return "Geners:"
        case .language:
            return "Language:"
        case .type:
            return "Type:"
        case .status:
            return "Status:"
        }
    }
}

class ShowDetailsViewModel {
    var showData: ShowsListModel
    
    init(showData: ShowsListModel) {
        self.showData = showData
    }

    func getInfoDescription(currentCell: ShowDetailsCells) -> String {
        switch currentCell {
        case .headerInfo:
            return ""
        case .geners:
            if let geners = showData.show?.genres, !geners.isEmpty {
                return geners.joined(separator: ",")
            } else {
                return "Not defined"
            }
        case .language:
            return showData.show?.language ?? "Not defined"
        case .type:
            return showData.show?.type ?? "Not defined"
        case .status:
            return showData.show?.status ?? "No Status"
        }
    }
}
