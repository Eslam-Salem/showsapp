//
//  ShowHeaderDataTableViewCell.swift
//
//  Created by Eslam Salem on 12/25/21.
//

import UIKit

class ShowHeaderDataTableViewCell: UITableViewCell {
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var imageActivityIndicator: UIActivityIndicatorView!
    
    func setDataToCell(with data: ShowsListModel) {
        titleLabel.text = data.show?.name
        descriptionLabel.text = data.show?.summary?.html2String
        setAvatarImage(imagePath: data.show?.image?.medium ?? "")
    }

    private func setAvatarImage(imagePath: String) {
        imageActivityIndicator.startAnimating()
        avatarImageView.loadImage(from: imagePath) {
            self.imageActivityIndicator.stopAnimating()
        }
    }
}
