//
//  ShowInfoTableViewCell.swift
//
//  Created by Eslam Salem on 12/25/21.
//

import UIKit

class ShowInfoTableViewCell: UITableViewCell {
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    
    func setDataToCell(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
