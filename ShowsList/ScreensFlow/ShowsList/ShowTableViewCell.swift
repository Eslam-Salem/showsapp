//
//  ShowTableViewCell.swift
//
//  Created by Eslam Salem on 12/24/21.
//

import UIKit
import Cosmos

class ShowTableViewCell: UITableViewCell {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var premieredLabel: UILabel!
    @IBOutlet private var runTimeLabel: UILabel!
    @IBOutlet private var ratingView: CosmosView!
    @IBOutlet private var showImageView: UIImageView!
    @IBOutlet private var imageActivityIndicator: UIActivityIndicatorView!
    var goToShowLinkHandler: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        configureDesign()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageActivityIndicator.isHidden = true
        showImageView.image = nil
    }
    
    @IBAction private func goToShowLink() {
        goToShowLinkHandler?()
    }

    func setDataToCell(with data: ShowsListModel) {
        titleLabel.text = data.show?.name ?? "No name"
        ratingView.rating = data.show?.rating?.average ?? 0
        configureRunTimeLabel(runTime: data.show?.runtime ?? 0)
        premieredLabel.text = "Premiered: " + "\(data.show?.premiered ?? "Not Premiered yet")"
        setShowImage(imagePath: data.show?.image?.medium ?? "")
    }

    private func configureRunTimeLabel(runTime: Int?) {
        var runTimeText = "Runtime: "
        if let runTime = runTime {
            runTimeText += "\(runTime)"
        } else {
            runTimeText += "-"
        }
        runTimeLabel.text = runTimeText
    }

    private func setShowImage(imagePath: String) {
        imageActivityIndicator.startAnimating()
        showImageView.loadImage(from: imagePath) {
            self.imageActivityIndicator.stopAnimating()
        }
    }

    private func configureDesign() {
        containerView.setShadow()
        showImageView.makeRounded()
    }
}
