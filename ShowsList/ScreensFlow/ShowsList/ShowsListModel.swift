//
//  ShowsListModel.swift
//
//  Created by Eslam Salem on 12/24/21.
//

import Foundation

struct RatingResponseObject: Decodable {
    var average: Double?
}

struct ImageResponseObject: Decodable {
    var medium: String?
}

struct ShowResponse: Decodable {
    var name: String?
    var summary: String?
    var language: String?
    var status: String?
    var type: String?
    var genres: [String]?
    var url: String?
    var runtime: Int?
    var premiered: String?
    var rating: RatingResponseObject?
    var image: ImageResponseObject?
}

struct ShowsListModel: Decodable {
    var show: ShowResponse?
}
