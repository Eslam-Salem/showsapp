//
//  ShowsListViewController.swift
//
//  Created by Eslam Salem on 12/24/21.
//

import UIKit

class ShowsListViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    private let viewModel = ShowsListViewModel()
    private let activityIndicator = ActivityIndicator()
    private var alertView: AlertHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Shows List"
        configureAlert()
        configureTableView()
        bindViewModel()
        requestShowsList()
    }

    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func configureAlert() {
        alertView = AlertHandler(presentingViewCtrl: self)
    }

    private func requestShowsList() {
        activityIndicator.displayForLoading(in: view)
        viewModel.requestShowsList()
    }

    private func bindViewModel() {
        viewModel.newDataToDisplayHandler = { [weak self] in
            self?.activityIndicator.dismiss()
            self?.handleEmptyState()
            self?.tableView.reloadData()
        }
        viewModel.errorHandler = { [weak self] message in
            self?.activityIndicator.dismiss()
            self?.alertView?.showErrorMessage(message: message)
        }
    }

    private func handleEmptyState() {
        if viewModel.dataSource.isEmpty {
            self.tableView.setEmptyView(message: "No Shows found")
        } else {
            self.tableView.removeEmptyState()
        }
    }

    private func openLink(url: String?) {
        guard let url = url,
            let url = URL(string: url),
            UIApplication.shared.canOpenURL(url)
        else {
            return
        }
        UIApplication.shared.open(url)
    }
}

extension ShowsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ShowDetailsConfiguration.navigateToShowDetails(
            show: viewModel.dataSource[indexPath.row],
            presentingViewController: self
        )
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ShowsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeCell() as ShowTableViewCell
        cell.setDataToCell(with: viewModel.dataSource[indexPath.row])
        cell.goToShowLinkHandler = { [weak self] in
            self?.openLink(
                url: self?.viewModel.dataSource[indexPath.row].show?.url
            )
        }
        return cell
    }
}
