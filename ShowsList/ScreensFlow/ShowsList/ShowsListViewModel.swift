//
//  ShowsListViewModel.swift
//
//  Created by Eslam Salem on 12/24/21.
//

import Foundation

class ShowsListViewModel {
    var errorHandler: ((_: String) -> Void)?
    var newDataToDisplayHandler: (() -> Void)?
    var dataSource = [ShowsListModel]()
    
    //MARK: -  functions

    func requestShowsList() {
        guard let url = URL(string: ShowsListAPIServices.showList.rawValue) else {
            errorHandler?("Not found: Something went worng!!!")
            return
        }
        APIClient.request(url: url, httpMethod: .get, responseType: [ShowsListModel].self) { [weak self] response, error in
            guard let response = response, error == nil else {
                self?.errorHandler?(error?.localizedDescription ?? "")
                return
            }
            self?.dataSource = response
            self?.newDataToDisplayHandler?()
        }
    }
}
